/**
 * Bài 1: Xuất 3 số theo thứ tự tăng dần
 *
 * Đầu vào: 3 số nguyên
 *
 * Các bước xử lý:
 * Bước 1: Tạo biến cho 3 số nguyên num1, num2, num3
 * Bước 2: Tạo biến cho kết quả result
 * Bước 3: Gán giá trị cho num1, num2, num3
 * Bước 4: So sánh num1 >= num2 và num1 >= num3, nếu đúng thì so sánh num2 > num3, nếu đúng thì xuất num3 -> num2 -> num1, nếu sai thì xuất num2 -> num3 -> num1
 * Bước 5: So sánh num2 >= num1 và num2 >= num3, nếu đúng thì so sánh num1 > num3, nếu đúng thì xuất num3 -> num1 -> num2, nếu sai thì xuất num1 -> num3 -> num2
 * Bước 6: So sánh num3 >= num1 và num3 >= num2, nếu đúng thì so sánh num1 > num2, nếu đúng thì xuất num2 -> num1 -> num3, nếu sai thì xuất num1 -> num2 -> num3
 * Bước 6: In kết quả ra console
 *
 * Đầu ra: Xuất 3 số theo thứ tự tăng dần */

function sort() {
  var num1 = document.getElementById("num-1").value * 1;
  var num2 = document.getElementById("num-2").value * 1;
  var num3 = document.getElementById("num-3").value * 1;

  if (num1 >= num2 && num1 >= num3) {
    if (num2 > num3) {
      document.getElementById(
        "result-1"
      ).innerText = `👉 ${num3}, ${num2}, ${num1}`;
    } else {
      document.getElementById(
        "result-1"
      ).innerText = `👉 ${num2}, ${num3}, ${num1}`;
    }
  } else if (num2 >= num1 && num2 >= num3) {
    if (num1 > num3) {
      document.getElementById(
        "result-1"
      ).innerText = `👉 ${num3}, ${num1}, ${num2}`;
    } else {
      document.getElementById(
        "result-1"
      ).innerText = `👉 ${num1}, ${num3}, ${num2}`;
    }
  } else if (num3 >= num1 && num3 >= num2) {
    if (num1 > num2) {
      document.getElementById(
        "result-1"
      ).innerText = `👉 ${num2}, ${num1}, ${num3}`;
    } else {
      document.getElementById(
        "result-1"
      ).innerText = `👉 ${num1}, ${num2}, ${num3}`;
    }
  }
}

/**
 * Bài 2: Chương trình "Chào hỏi"
 *
 * Đầu vào: Người sử dụng máy
 *
 * Các bước xử lý:
 * Bước 1: Tạo biến cho người sử dụng máy person
 * Bước 2: Sử dụng switch case theo value của person để xuất ra lời chào tương ứng: B -> Bố, M -> Mẹ, A -> Anh Trai, E -> Em Gái
 * Bước 3: In kết quả ra console
 *
 * Đầu ra: Đưa ra lời chào phù hợp */

function sayHello() {
  var person = document.getElementById("person").value;
  switch (person) {
    case "B":
      document.getElementById("result-2").innerText = "👉 Xin chào Bố!";
      break;
    case "M":
      document.getElementById("result-2").innerText = "👉 Xin chào Mẹ!";
      break;
    case "A":
      document.getElementById("result-2").innerText = "👉 Xin chào Anh Trai!";
      break;
    case "E":
      document.getElementById("result-2").innerText = "👉 Xin chào Em Gái!";
      break;
    default:
      document.getElementById("result-2").innerText =
        "👉 Xin chào Người lạ ơi!";
      break;
  }
}

/**
 * Bài 3: Đếm số chẵn lẻ
 *
 * Đầu vào: 3 số nguyên
 *
 * Các bước xử lý:
 * Bước 1: Tạo biến cho 3 số nguyên num1, num2, num3
 * Bước 2: Tạo biến đếm count
 * Bước 3: Kiểm tra từng số có chia hết cho 2 không. Nếu có thì đó là số chẵn và tăng biến count thêm 1. Nếu không thì đó là số lẻ.
 * Bước 4: count là số lượng số chẵn, 3 - count là số lượng số lẻ
 * Bước 5: In kết quả ra console
 *
 * Đầu ra: Xuất ra có bao nhiêu số lẻ và bao nhiêu số chẵn */

function count() {
  var num1 = document.getElementById("number-1").value * 1;
  var num2 = document.getElementById("number-2").value * 1;
  var num3 = document.getElementById("number-3").value * 1;
  var count = 0;
  if (num1 % 2 == 0) {
    count++;
  }
  if (num2 % 2 == 0) {
    count++;
  }
  if (num3 % 2 == 0) {
    count++;
  }
  document.getElementById("result-3").innerText = `👉 Có ${count} số chẵn, ${
    3 - count
  } số lẻ`;
}

/**
 * Bài 4: Đoán hình tam giác
 *
 * Đầu vào: 3 cạnh của tam giác
 *
 * Các bước xử lý:
 * Bước 1: Tạo biến cho 3 cạnh tam giác edge1, edge2, edge3
 * Bước 2: Kiểm tra 3 cạnh tam giác phải thỏa mãn edge1 + edge2 > edge3, edge1 + edge3 > edge2, edge2 + edge3 > edge1
 * Bước 3: Nếu tam giác có 3 cạnh bằng nhau thì xuất ra tam giác đều
 * Bước 4: Nếu tam giác có 2 cạnh bằng nhau thì xuất ra tam giác cân
 * Bước 5: Nếu edge1^2 + edge2^2 = edge3^2, edge1^2 + edge3^2 = edge2^2, edge2^2 + edge3^2 = edge1^2 thì xuất ra tam giác vuông
 * Bước 6: In kết quả ra console
 *
 * Đầu ra: Cho biết tam giác gì */

function guess() {
  var edge1 = document.getElementById("edge-1").value * 1;
  var edge2 = document.getElementById("edge-2").value * 1;
  var edge3 = document.getElementById("edge-3").value * 1;

  if (
    edge1 + edge2 <= edge3 ||
    edge1 + edge3 <= edge2 ||
    edge2 + edge3 <= edge1
  ) {
    window.alert("Dữ liệu không hợp lệ");
    return;
  }
  if (edge1 == edge2 && edge1 == edge3) {
    document.getElementById("result-4").innerText = "👉 Hình tam giác đều";
  } else if (edge1 == edge2 || edge1 == edge3 || edge2 == edge3) {
    document.getElementById("result-4").innerText = "👉 Hình tam giác cân";
  } else if (
    edge1 * edge1 + edge2 * edge2 == edge3 * edge3 ||
    edge1 * edge1 + edge3 * edge3 == edge2 * edge2 ||
    edge2 * edge2 + edge3 * edge3 == edge1 * edge1
  ) {
    document.getElementById("result-4").innerText = "👉 Hình tam giác vuông";
  } else {
    document.getElementById("result-4").innerText = "👉 Một loại tam giác khác";
  }
}
